const router = require('express').Router();

// const customerController = require('./src/controllers/customerController');
const customerController = require('../controller/customerController');

router.get('/', customerController.list);
router.post('/add', customerController.save);
router.get('/update/:id', customerController.edit);
router.post('/update/:id', customerController.update);
router.get('/delete/:id', customerController.delete);

module.exports = router;