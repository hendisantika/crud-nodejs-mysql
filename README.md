# CRUD NodeJS MySQL

Run this project by this command : 
1. Clone this repo by this command : `git clone git@gitlab.com:hendisantika/crud-nodejs-mysql.git`
2. Go to directory : `cd crud-nodejs-mysql`
3. Install dependencies : `npm install`
4. Run sql script in database folder
5. Run the app --> `nodemon index.js` or `node index.js`

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Customer Data

![Add New Customer Data](img/add.png "Add New Customer Data")

Edit Customer Data

![Edit Customer Data](img/edit.png "Edit Customer Data")

List Customer Data

![List Customer Data](img/list.png "List Customer Data")